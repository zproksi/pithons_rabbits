#include "stdafx.h"
#include "GamePrimitive.h"


CGamePrimitive::CGamePrimitive(const int aColor, const char aSymbol, const uint32_t zOrder)
   :m_color(aColor)    // my color on the screen
   , m_Symbol(aSymbol) // my draw Symbol
   , m_zOrder(zOrder)  // my Z order - For Draw
{
}


CGamePrimitive::~CGamePrimitive()
{
}
