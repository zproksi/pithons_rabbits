#include "stdafx.h"
#include "Generator.h"



Generator::Generator():m_generator(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()))
{
}

uint32_t Generator::Generate(const uint32_t aFrom, const uint32_t aTo)
{
	std::uniform_int_distribution<uint32_t> distribution(aFrom, aTo);
	return distribution(m_generator);
}


Generator::~Generator()
{
}
