#ifndef  GAME_SETTINGS_W9427609lsJ
#define  GAME_SETTINGS_W9427609lsJ

/// �������� �������

// ������ ��� �� ������� �� 1 ������ ������ 1000 ����������
#define SPEED_RATIO  1000

///  per milliseconds
#define  RABBIT_SPEED   1000

///  per milliseconds
#define  PYTHON_SPEED   500

#define  MIN_RABBITS_COUNT 20
#define  MAX_RABBITS_COUNT 60

#define  MIN_PYTHONS_COUNT 3
#define  MAX_PYTHONS_COUNT 5

#define GAME_SCREEN_WIDTH 80
#define GAME_SCREEN_HEIGHT 40



///  8 points per second
#define  BIRD_SPEED 8000

#define  BIRDS_COUNT_MIN   3
#define  BIRDS_COUNT_MAX   7

#define  BIRD_CHANGEDIRECTION_SECONDS_MIN   8
#define  BIRD_CHANGEDIRECTION_SECONDS_MAX   26


// Game primitives zorder
enum ZORDER_GP : uint32_t
{

   zorder_Bird = UINT32_MAX,
};

// Game primitives character
enum SYMBOL_GP : char
{

   symbol_Bird = 'V',
};


enum COLOR_GP : uint16_t
{
   color_Bird = FOREGROUND_BLUE | FOREGROUND_INTENSITY,
};






#endif // GAME_SETTINGS_W9427609lsJ


