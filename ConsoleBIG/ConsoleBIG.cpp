// ConsoleBIG.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Allignment.h"
#include "MrBlack.h"
#include "DrawSymbolRealization.h"
#include "GameScreen.h"
#include "Bird.h"



int _tmain(int argc, _TCHAR* argv[])
{
   HANDLE out_handle = GetStdHandle(STD_OUTPUT_HANDLE);
   COORD crd = { GAME_SCREEN_WIDTH, GAME_SCREEN_HEIGHT };
   SMALL_RECT src = {0, 0, crd.X, crd.Y};
   SetConsoleWindowInfo(out_handle, true, &src);
   SetConsoleScreenBufferSize(out_handle, crd);
   SetConsoleTitleW(L"MyGame");


   GamePrimitivesCollection v_primitives;

   CBird::FillTheField(&v_primitives); // add birds objects

   GameScreen v_screen;

   do
   {
      v_screen.Reset();
      for (auto& primitive : v_primitives)
      {
         primitive->OnTime(0.001);
         primitive->Draw(&v_screen);
      }
      v_screen.Draw(out_handle);
      ::Sleep(1);
   } while (true);


	system("pause");
	return 0;
}

