#ifndef  GAMESCREEN_WEOITHSKDG240597
#define  GAMESCREEN_WEOITHSKDG240597

class GameScreen
{
protected:
	CHAR_INFO m_screenData[GAME_SCREEN_WIDTH * GAME_SCREEN_HEIGHT];
public:
	GameScreen();
	~GameScreen();

	void Set(const size_t atX, const size_t atY, const CHAR_INFO& toSet);
	CHAR_INFO Get(const size_t atX, const size_t atY);

	/// set field empty again
	void Reset();
	void Draw(HANDLE aConsole);
};


#endif //GAMESCREEN_WEOITHSKDG240597
