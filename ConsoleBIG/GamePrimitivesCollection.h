#ifndef DSFLGKJSDFGGAMEPRIMITIVESCOLLECTION
#define DSFLGKJSDFGGAMEPRIMITIVESCOLLECTION

class CGamePrimitive;

typedef std::shared_ptr<CGamePrimitive> GAME_PRIMITIVE;
typedef std::vector< GAME_PRIMITIVE > GAME_PRIMITIVES_COLLECTION;
typedef std::vector< GAME_PRIMITIVE >::iterator ITERATOR_GPC;




class GamePrimitivesCollection
{
public:

   /// �������� ����� ��������
   void AddGamePrimitive(const GAME_PRIMITIVE& toAdd);

   /// toFill ���������
   /// ���� ���� aGPType
   void GetGamePrimitives(GAME_PRIMITIVES_COLLECTION& toFill, const char aGPType);

   /// toEnter ����� ������
   /// ���� �������� aGPType
   void ReplaceGamePrimitives(const char aGPType, GAME_PRIMITIVES_COLLECTION& toEnter);


   ITERATOR_GPC begin()
   {
      return m_collection.begin();
   };
   ITERATOR_GPC end()
   {
      return m_collection.end();
   };


protected:

   GAME_PRIMITIVES_COLLECTION m_collection;

};


#endif // DSFLGKJSDFGGAMEPRIMITIVESCOLLECTION
