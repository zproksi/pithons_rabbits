#include "stdafx.h"
#include "Bird.h"
#include "GameScreen.h"


CBird::CBird():CGamePrimitive(COLOR_GP::color_Bird , SYMBOL_GP::symbol_Bird, ZORDER_GP::zorder_Bird)
{
   m_where.X = g_Generator.Generate(0, GAME_SCREEN_WIDTH);
   m_where.Y = g_Generator.Generate(0, GAME_SCREEN_HEIGHT);

   m_nowX = static_cast<double>(m_where.X);
   m_nowY = static_cast<double>(m_where.Y);

   GenerateSpeedForTheBird();
}

void CBird::GenerateSpeedForTheBird()
{
   // generate next moment of changing direction
   m_ChangeDirectionTime = static_cast<decltype(m_ChangeDirectionTime)>(g_Generator.Generate(BIRD_CHANGEDIRECTION_SECONDS_MIN * 10000, BIRD_CHANGEDIRECTION_SECONDS_MAX * 10000)) / 10000.;

   // generate new speed
   m_speedX = static_cast<decltype(m_speedX)>(g_Generator.Generate(0, BIRD_SPEED)) - static_cast<decltype(m_speedX)>(BIRD_SPEED / 2);
   m_speedY = static_cast<decltype(m_speedY)>(g_Generator.Generate(0, BIRD_SPEED)) - static_cast<decltype(m_speedY)>(BIRD_SPEED / 2);
}


CBird::~CBird()
{
}

void CBird::Draw(GameScreen* const aScreen)
{
   CHAR_INFO cToSet;
   cToSet.Char.UnicodeChar = 0;
   cToSet.Char.AsciiChar = m_Symbol;
   cToSet.Attributes = m_color;

   aScreen->Set(m_where.X, m_where.Y, cToSet);
}

void CBird::OnTime(double a_dt)
{
   m_nowX += a_dt * static_cast<decltype(m_nowX)>(m_speedX) / 1000.;
   m_nowY += a_dt * static_cast<decltype(m_nowY)>(m_speedY) / 1000.;

   m_where.X = static_cast<decltype(m_where.X)>(m_nowX);
   m_where.Y = static_cast<decltype(m_where.Y)>(m_nowY);

   while (m_nowX < 0.)
   {
      m_nowX += GAME_SCREEN_WIDTH;
   }
   while (m_nowX >= GAME_SCREEN_WIDTH)
   {
      m_nowX -= GAME_SCREEN_WIDTH;
   }


   while (m_nowY >= GAME_SCREEN_HEIGHT)
   {
      m_nowY -= GAME_SCREEN_HEIGHT;
   }
   while (m_nowY < 0.)
   {
      m_nowY += GAME_SCREEN_HEIGHT;
   }
   // it's possible we need to change direction
   m_ChangeDirectionTime -= a_dt;
   if (m_ChangeDirectionTime < 0.)
   {
      GenerateSpeedForTheBird();
   }
}


void CBird::FillTheField(GamePrimitivesCollection* const acollection)
{
   auto nBirds = g_Generator.Generate(BIRDS_COUNT_MIN, BIRDS_COUNT_MAX);
   while (nBirds-- > 0)
   {
      acollection->AddGamePrimitive(GAME_PRIMITIVE(new CBird));
   }

}


