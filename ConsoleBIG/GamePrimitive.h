#ifndef  GAME_SETTINGS_W9434897
#define  GAME_SETTINGS_W9434897

class GameScreen;

class CGamePrimitive
{
protected:
   CGamePrimitive(const int aColor, const char aSymbol, const uint32_t zOrder);

public:
   CGamePrimitive(COORD aWhere) :m_where(aWhere){}
   virtual ~CGamePrimitive();

   virtual void Draw(GameScreen* const aScreen) = 0; // time to draw me
   virtual void OnTime( double a_dt ) = 0; // time to move me if necessary

   virtual char EntityType()const{return m_Symbol;} // this is inline
   virtual uint32_t ZOrder()const{return m_zOrder;} // this is inline


protected:
   COORD m_where; // Where am I
   int m_color = 0; // my color on the screen
   char m_Symbol = ' '; // my draw Symbol
   uint32_t m_zOrder = 0; // my Z order - For Draw
};



#endif // GAME_SETTINGS_W9434897
