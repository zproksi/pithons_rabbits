#include "stdafx.h"
#include "GamePrimitivesCollection.h"
#include "GamePrimitive.h"
#include <initializer_list>


void GamePrimitivesCollection::AddGamePrimitive(const GAME_PRIMITIVE& toAdd)
{
   m_collection.push_back(toAdd);
}

void GamePrimitivesCollection::GetGamePrimitives(GAME_PRIMITIVES_COLLECTION& toFill, const char aGPType)
{
   toFill.clear();
   for (auto it : m_collection)
   {
      if (it->EntityType() == aGPType)
      {
         toFill.emplace_back(it);
      }
   }
}

void GamePrimitivesCollection::ReplaceGamePrimitives(const char aGPType, GAME_PRIMITIVES_COLLECTION& toEnter)
{
   decltype(m_collection) newCollection;

   for (auto& it : m_collection)
   {
      if (it->EntityType() != aGPType)
      {
         newCollection.emplace_back(it);
      }
   }

   std::swap(newCollection, m_collection);

   m_collection.reserve(m_collection.size() + toEnter.size()); // Reserve space first
   m_collection.insert(m_collection.end(), std::make_move_iterator(toEnter.begin()),
      std::make_move_iterator(toEnter.end()));
}
