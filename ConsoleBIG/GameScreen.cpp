#include "stdafx.h"
#include "GameScreen.h"


GameScreen::GameScreen()
{
   Reset();
}


GameScreen::~GameScreen()
{
}

void GameScreen::Set(const size_t atX, const size_t atY, const CHAR_INFO& toSet)
{
   m_screenData[(atX % GAME_SCREEN_WIDTH) + (atY % GAME_SCREEN_HEIGHT) * GAME_SCREEN_WIDTH] = toSet;
}
CHAR_INFO GameScreen::Get(const size_t atX, const size_t atY)
{
   return m_screenData[(atX % GAME_SCREEN_WIDTH) + (atY % GAME_SCREEN_HEIGHT) * GAME_SCREEN_WIDTH];
}

/// set field empty again
void GameScreen::Reset()
{
	memset(m_screenData, 0, sizeof(m_screenData));
	for (auto& v_ci : m_screenData)
	{
		v_ci.Char.AsciiChar = ' ';
	}
}
void GameScreen::Draw(HANDLE aConsole)
{
	const COORD dwBufferSize = {GAME_SCREEN_WIDTH, GAME_SCREEN_HEIGHT};
	const COORD dwBufferCoord = {0, 0};
	SMALL_RECT v_WriteRegion = { 0, 0, GAME_SCREEN_WIDTH, GAME_SCREEN_HEIGHT };

	WriteConsoleOutput(aConsole,
		m_screenData,
		dwBufferSize,
		dwBufferCoord,
		&v_WriteRegion
		);
}


