#ifndef WSRLGKHJWRLKKSDFGJSLGYOIERJY304598
#define WSRLGKHJWRLKKSDFGJSLGYOIERJY304598

#include "GamePrimitivesCollection.h"
#include "GamePrimitive.h"


class CBird : public CGamePrimitive
{
protected:
   double m_speedX;
   double m_speedY;

   double m_nowX;
   double m_nowY;

   double m_ChangeDirectionTime = 0.;
public:
   CBird();
   ~CBird();

   void Draw(GameScreen* const aScreen)override;
   void OnTime(double a_dt)override;

   // method for create birds
   static void FillTheField(GamePrimitivesCollection* const acollection);


protected:
   // when bird change direction of the fly
   void GenerateSpeedForTheBird();
};


#endif // !WSRLGKHJWRLKKSDFGJSLGYOIERJY304598
