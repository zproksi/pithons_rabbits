#pragma once
#include "GamePrimitive.h"
class DrawSymbolRealization : public CGamePrimitive
{
public:
	DrawSymbolRealization(COORD aWhere);
	virtual ~DrawSymbolRealization();

   virtual void Draw(GameScreen* const aScreen)override;
	virtual void OnTime(double a_dt)override;
};

